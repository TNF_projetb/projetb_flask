### Readme Flask

##### Emplacement du virtual env python avec flask, uwsgi, gunicorn
/home/vagrant/projeta_back/back_venv

##### Emplacement des fichiers de l'api flask:
/home/vagrant/projeta_back/api

pip install 'flake8==3.9.2' 'flake8_flask' 'flake8-junit-reporter'



  script:
    - flake8 $CI_PROJECT_DIR/api/ --output-file=$CI_PROJECT_DIR/test/flake8.txt
    - flake8_junit $CI_PROJECT_DIR/test/flake8.txt $CI_PROJECT_DIR/test/flake8_junit.xml

  artifacts:
    when: always
    paths:
      - $CI_PROJECT_DIR/test/flake8_junit.xml
    reports:
      junit: flake8_junit.xml